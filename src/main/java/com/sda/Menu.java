package com.sda;

import com.sda.dao.VeterinarianDao;
import com.sda.model.Veterinarian;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Menu {
    private static final Scanner scanner = new Scanner(System.in);
    private VeterinarianDao vetDao = new VeterinarianDao();

    public void startApp() {
        System.out.println("Pet shop menu.");
        displayMenu();
        exploreMenu();

    }

    private String getUserInput() {
        return scanner.next();
    }

    private void displayMenu() {
        System.out.println("0. Exit\n" +
                "1. Create\n" +
                "2. Update\n" +
                "3. Delete\n" +
                "4. List/Find");
    }

    private void displayCreateSubmenu() {
        System.out.println("0. Create a veterinarian\n" +
                "1. Create a pet\n" +
                "2.Create a consult");
    }

    private void exitFromCreateAVetMenu() {
        System.out.print("0. Exit the application\n" +
                "1. Return to the create menu\n" +
                "2. Return to the main menu\n");
    }

    private void exploreMenu() {
        String userInput = getUserInput();
        if (isValidNumberBetween0And4(userInput)) {
            int userInputAsInt = Integer.parseInt(userInput);
            switch (userInputAsInt) {
                case 0:
                    System.out.println("Thank you!");
                    System.exit(0);
                    break;
                case 1:
                    case1();
                    break;
            }
        } else {
            System.out.println("Invalid input!");
            displayMenu();
            exploreMenu();
        }
    }


    private boolean isValidNumberBetween0And4(String userInput) {
        return userInput.matches("^[0-4]{1}$");
    }

    private boolean isValidNumberBetween0And2(String userInput) {
        return userInput.matches("^[0-2]{1}$");
    }

    private List<String> displayCreateAVetSubmenu() {
        List<String> vetAttributes = new ArrayList<>();
        System.out.print("Enter the veterinarian first name: ");
        String firstName = getUserInput();
        System.out.print("Enter the veterinarian last name: ");
        String lastName = getUserInput();
        System.out.print("Enter the veterinarian address: ");
        String address = getUserInput();
        System.out.print("Enter the veterinarian speciality: ");
        String speciality = getUserInput();

        vetAttributes.add(firstName);
        vetAttributes.add(lastName);
        vetAttributes.add(address);
        vetAttributes.add(speciality);

        return vetAttributes;
    }

    private void case1() {
        displayCreateSubmenu();
        String userInput = getUserInput();
        if (isValidNumberBetween0And2(userInput)) {
            switch (Integer.parseInt(userInput)) {
                case 0:
                    List<String> vetAttributes = displayCreateAVetSubmenu();
                    createAVet(vetAttributes);
                    exitMenu();
                    break;
            }
        } else {
            System.out.println("Invalid input!");
            case1();
        }
    }

    private void createAVet(List<String> vetAttributes) {
        Veterinarian vet = new Veterinarian(vetAttributes.get(0),
                vetAttributes.get(1), vetAttributes.get(2), vetAttributes.get(3));
        vetDao.createEntity(vet);
        System.out.println(vetDao.findAll(Veterinarian.class));
        exitMenu();
    }

    private void exitMenu() {
        exitFromCreateAVetMenu();
        String userInput = getUserInput();
        if (isValidNumberBetween0And2(userInput)) {
            int userInputAsInt = Integer.parseInt(userInput);
            switch (userInputAsInt) {
                case 0:
                    System.out.println("Thank you!");
                    System.exit(0);
                    break;
                case 1:
                    case1();
                    break;
                case 2:
                    startApp();
                    break;
            }
        } else {
            System.out.println("Invalid input!");
            exitMenu();
        }
    }

    private static Date parseDate(String date) {
        try {
            return (Date) new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
