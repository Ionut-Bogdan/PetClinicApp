package com.sda.dao;

import com.sda.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public abstract class GenericDao<T> {

    /**
     * returns a generic type object from the database based on the object
     * id pasted by the user. If the id doesn't exist it returns null.
     *
     * @param c  -> generic type class.
     * @param id -> the id of the object that we want to fetch from the
     *           database.
     * @return -> the generic type object or null.
     */
    public T findById(Class<T> c, long id) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            return (T) session.find(c, id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    /**
     * returns a list of generic type objects from the database based on the
     * class type.
     *
     * @param c -> generic type class.
     * @return
     */
    public List<T> findAll(Class<T> c) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            return session.createQuery("from " + c.getName(), c).list();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Select all failed!", e);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public void createEntity(T entity) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(entity);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public void updateEntity(T entity) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.update(entity);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public void deleteEntity(T entity) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.delete(entity);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
