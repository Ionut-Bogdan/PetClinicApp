package com.sda.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
public class Pet {

    @Id
    @GeneratedValue
    private Long id;

    private String race;

    @Column(name = "birth_date")
    private java.sql.Date birthDate;

    @Column(name = "is_vaccinated")
    private boolean isVaccinated;

    @Column(name = "owner_name")
    private String ownerName;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    private Date createDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_date")
    private Date updateDate;

    @OneToMany(mappedBy = "pet", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @ToString.Exclude
    private List<Consult> consults;

    public Pet(String race, String sDate, boolean isVaccinated,
               String ownerName) {
        this.race = race;
        this.birthDate = java.sql.Date.valueOf(sDate);
        this.isVaccinated = isVaccinated;
        this.ownerName = ownerName;
    }
}
