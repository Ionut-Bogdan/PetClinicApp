package com.sda.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
public class Veterinarian {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "first_name")
    @Size(min = 3)
    private String firstName;

    @Column(name = "last_name")
    @Size(min = 3)
    private String lastName;

    private String address;
    private String speciality;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    private Date createDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_date")
    private Date updateDate;

    @OneToMany(mappedBy = "veterinarian", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @ToString.Exclude
    private List<Pet> pets;

    @OneToMany(mappedBy = "veterinarian", cascade = CascadeType.ALL)
    @ToString.Exclude
    private List<Consult> consults;

    public Veterinarian(String firstName, String lastName, String address,
                        String speciality) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.speciality = speciality;
    }
}
