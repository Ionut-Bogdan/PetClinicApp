package com.sda.util;

import com.github.javafaker.Faker;
import com.sda.dao.VeterinarianDao;
import com.sda.model.Veterinarian;

import java.util.Arrays;
import java.util.List;

public class GenerateDbDate {

    private static Faker faker = new Faker();

    public void generateFakeEntriesForVet() {
        List<String> vetSpeciality = Arrays.asList("Canine", "Feline", "Bird");
        VeterinarianDao veterinarianDao = new VeterinarianDao();

        for (int i = 0; i < 1000; i++) {
            String firstName = faker.name().firstName();
            String lastName = faker.name().lastName();
            String address = faker.address().city();
            String speciality = vetSpeciality.get(i % 3);
            Veterinarian veterinarian = new Veterinarian(firstName, lastName,
                    address, speciality);
            veterinarianDao.createEntity(veterinarian);
        }
    }
}
